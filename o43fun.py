import ipaddr

def option43(controllers, ips):
    '''Cisco-Specific Option 43 Configuration
    Blah blah something about how this works....
    '''
    return 'f1%s%s' % ('%02X' % (controllers * 4), ''.join(iphexify(ips)))


def iphexify(ips):
    '''Ip Hexification
    Returns the Hex value of all of the ips
    '''
    return [''.join(['%02X' % int(i) for i in ip.split('.')]) for ip in ips]


def wlc_dhcp(pool, network, netmask, gateway, dns, domain, lease_d,
               lease_h, lease_s, reservation_b, reservation_e):
    '''DHCP Scope Generator
    in Cisco WLC formatting
    '''
    seconds = (int(lease_d) * 86400) + (int(lease_h) * 3600) + int(lease_s)
    return '\n'.join([
        'config dhcp create-scope %s' % pool,
        'config dhcp default-router %s %s' % (pool, gateway),
        'config dhcp address-pool %s %s %s' % (pool, reservation_b, reservation_e),
        'config dhcp dns-servers %s %s' % (pool, ' '.join([x.strip('\r') for x in dns])),
        'config dhcp network %s %s %s' % (pool, network, netmask),
        'config dhcp lease %s %s' % (pool, seconds),
    ])

def cisco_dhcp(pool, network, netmask, gateway, dns, domain, lease_d,
               lease_h, lease_s, reservation_b, reservation_e,
               controllers, ips):
    '''DHCP Scope Generator
    in Cisco formatting
    '''
    return '\n'.join([
        'ip dhcp excluded-address %s %s' % (reservation_b, reservation_e),
        'ip dhcp pool %s' % pool,
        'network %s %s' % (network, netmask),
        'default-router %s' % gateway,
        'domain-name %s' % domain,
        'lease %s %s %s' % (lease_d, lease_h, lease_s),
        'dns-server %s' % (' '.join([x.strip('\r') for x in dns])),
        'option 43 hex %s' % option43(controllers, ips),
    ])

def microsoft_dhcp(pool, network, netmask, gateway, dns, domain, lease_d,
               lease_h, lease_s, reservation_b, reservation_e,
               controllers, ips):
    '''DHCP Scope Generator
    in Microsoft formatting
    '''
    seconds = (int(lease_d) * 86400) + (int(lease_h) * 3600) + int(lease_s)
    scope_b = ipaddr.IPv4Address(network) + 1
    scope_e = ipaddr.IPv4Network('/'.join([network,netmask])).broadcast - 1
    return '\n'.join([
        'netsh dhcp server add scope %s %s \"%s\" \"%s\"' % 
        (network, netmask, pool, pool),
        'netsh dhcp server scope %s add iprange %s %s' % (network, scope_b, 
            scope_e),
        'netsh dhcp server scope %s add excluderange %s %s' % 
        (network, reservation_b, reservation_e),
        'netsh dhcp server scope %s set optionvalue 003 IPADDRESS %s'
         % (network, gateway),
         'netsh dhcp server scope %s set optionvalue 015 STRING \"%s\"'
         % (network, domain),
        'netsh dhcp server scope %s set optionvalue 51 DWORD %d'
        % (network, seconds),
        '\n'.join(['netsh dhcp server scope %s set optionvalue 006 IPADDRESS %s' % (network, x) for x in dns]),
        'netsh dhcp server scope %s set optionvalue 43 BINARY %s'
        % (network, option43(controllers, ips)),
    ])
