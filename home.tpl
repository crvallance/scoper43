<html>
  <head>
    <title>DHCP scope Generator (with option 43) </title>
    <style type="text/css">
    div{float: left; width: 30%; margin-right: 2%;}
    label{display: block;}
    textarea{margin-bottom: 15px;}
    span{display:block;}
    #output {
      margin: 10px; 
      /*border: 1px solid; */
      padding: 10px; 
      width: 70%; 
      float: right; 
      position: fixed; 
      right: 10px; 
      overflow: auto;
    }
    </style>
  </head>
  <body>
    <form name="config_out_submit" action="/" method="post">
      <div>
        <label>DHCP Server Type : 
        <select name="servertype">
          <option value="ms">Microsoft Server</option>
          <option value="cisco">Cisco IOS</option>
          <option value="wlc">Cisco WLC</option>
        </select>
      </label><br>          
        <label>Enter Number of Controllers : 
        <select name="controllers">
          <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
          <option value="4">Four</option>
          <option value="5">Five</option>
        </select>
      </label><br>
      <label>Enter IPs (one per line)</label>
      <textarea name="ip_addresses"></textarea>
      <label>Pool Name (No Spaces)</label>
      <textarea name="pool" style="width:254px; height:24px;"></textarea>
      <label>Address Exclusion Begin</label>
      <textarea name="reservations_b" style="width:254px; height:24px;"></textarea>
      <label>Address Exclusion End</label>
      <textarea name="reservations_e" style="width:254px; height:24px;"></textarea>
      <label>Network</label>
      <textarea name="network" style="width:254px; height:24px;"></textarea>
      <label>Netmask</label>
      <textarea name="netmask" style="width:254px; height:24px;"></textarea>
      <label>Gateway</label>
      <textarea name="gateway" style="width:254px; height:24px;"></textarea>
      <label>Domain</label>
      <textarea name="domain" style="width:254px; height:24px;"></textarea>
      <label>Lease Time (Days)</label>
      <textarea name="lease_d" style="width:254px; height:24px;"></textarea>
      <label>Lease Time (Hours)</label>
      <textarea name="lease_h" style="width:254px; height:24px;"></textarea>
      <label>Lease Time (Minutes)</label>
      <textarea name="lease_s" style="width:254px; height:24px;"></textarea>
      <label>DNS Server (one IP per line)</label>
      <textarea name="dns"></textarea>
      <label><input type="submit" value="DHCP ME"/><label><br>
      </div>
      <div>
      <!--Option 60 (For Microsoft Servers Only)<br><br>
      <select multiple name="option60" style="width:275px; height:395px;">
      <option value="Airespace.AP1200">Cisco Aironet 1000 series</option>
      <option value="Cisco AP c1040">Cisco Aironet 1040 series</option>
      <option value="Cisco AP c1100">Cisco Aironet 1100 series</option>
      <option value="Cisco AP c1130">Cisco Aironet 1130 series</option>
      <option value="Cisco AP c1140">Cisco Aironet 1140 series</option>
      <option value="Cisco AP c1200">Cisco Aironet 1200 series</option>
      <option value="Cisco AP c1200">Cisco Aironet 1230 series</option>
      <option value="Cisco AP c1240">Cisco Aironet 1240 series</option>
      <option value="Cisco AP c1250">Cisco Aironet 1250 Series</option>
      <option value="Cisco AP c1260">Cisco Aironet 1260 Series</option>
      <option value="Cisco AP c1310">Cisco Aironet 1300 series</option>
      <option value="Cisco AP c15001">Any 1500 Series AP that runs 4.1 software</option>
      <option value="Cisco AP.OAP15002">1500 OAP AP that runs 4.0 software</option>
      <option value="Cisco AP.LAP15053">1505 Model AP that runs 4.0 software</option>
      <option value="Cisco AP.LAP15104">1510 Model AP that runs 4.0 software</option>
      <option value="Airespace.AP12005">Any 1500 Series AP that runs 3.2 software</option>
      <option value="Cisco AP c1520">Cisco Aironet 1520 series</option>
      <option value="Cisco AP c1550">Cisco Aironet 1550 series</option>
      <option value="Cisco Bridge/AP/WGB c3201">Cisco 3201 Lightweight Access Point</option>
      <option value="Cisco AP c520">Cisco 521 Wireless Express Access Point</option>
      <option value="Cisco AP801">AP801 (embedded in 86x/88x series ISRs</option>
      <option value="Cisco AP c3500">Cisco Aironet 3500 Series</option>
      <option value="Cisco AP c3600">Cisco Aironet 3600 Series</option>
      <option value="Cisco AP802">AP802 (embedded in 88x series ISRs</option>
    </select><-->
      </div>
    </form>
  </div>
<div id="output">
  %if config_out:
  <label>DCHP Configuration</label>
  <pre>
{{ config_out }}
  </pre>
  %end
</div>
  </body>
</html>
