#!/usr/bin/env python
from bottle import Bottle, run, template, request
import os, sys, webbrowser
import o43fun

app = Bottle()

@app.get('/')
@app.post('/')
def compute_config():
    config_out = None  # Setting this to Nothing because we use it in the template
    # only perform the computation if we receive a POST with the values.
    if request.method == 'POST':
        # First thing we need to do is check to see that there are values set
        # for both the controllers field and that there is at least one IP
        # set.
        if request.forms.get('controllers') and\
           request.forms.get('ip_addresses'):
            controllers = int(request.forms.get('controllers'))
            ips = request.forms.get('ip_addresses').split('\n')
        else:
            # If nothing is set, then just return the page and let the user
            # know that they didnt actually input anything.
            return template('home', config_out='Invalid Inputs')

        # Get a bunch of values from our form
        pool = request.forms.get('pool')
        reservation_b = request.forms.get('reservations_b')
        reservation_e = request.forms.get('reservations_e')
        network = request.forms.get('network')
        netmask = request.forms.get('netmask')
        gateway = request.forms.get('gateway')
        domain = request.forms.get('domain')
        lease_d = request.forms.get('lease_d')
        lease_h = request.forms.get('lease_h')
        lease_s = request.forms.get('lease_s')
        dns = request.forms.get('dns').split('\n')
        
        # Save this for later if feature gets put back in
        #option60 = request.forms.getall('option60')
        
        # Pull server 
        if request.forms.get('servertype') == "ms":
            config_out = o43fun.microsoft_dhcp(pool, network, netmask, gateway,
             dns, domain, lease_d, lease_h, lease_s, reservation_b, 
             reservation_e, controllers, ips)
        elif request.forms.get('servertype') == 'cisco':
            config_out = o43fun.cisco_dhcp(pool, network, netmask, gateway,
             dns, domain, lease_d, lease_h, lease_s, reservation_b, 
             reservation_e, controllers, ips)
        elif request.forms.get('servertype') == 'wlc':
            config_out = o43fun.wlc_dhcp(pool, network, netmask, gateway,
             dns, domain, lease_d, lease_h, lease_s, reservation_b,
             reservation_e)

    # Return the template with the config_out line (or maybe nothing at all ;)
    return template('home', config_out=config_out)

if __name__ == '__main__':
    # If we are running the script directly, then start this biotch up!
    if getattr(sys, 'frozen', None):
      basedir = sys._MEIPASS
    else:
      basedir = os.path.dirname(__file__)
    os.chdir(basedir)  
    webbrowser.open_new('http://localhost:10375')
    run(app=app,host="localhost", port=10375)